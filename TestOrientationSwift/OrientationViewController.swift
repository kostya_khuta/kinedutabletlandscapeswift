//
//  OrientationViewController.swift
//  TestOrientationSwift
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

import UIKit

class OrientationViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        UIApplication.sharedApplication().statusBarHidden = true
        
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft || UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeRight {
            
            let landScapeVC = self.storyboard!.instantiateViewControllerWithIdentifier("landscape")
            landScapeVC.view.autoresizingMask = UIViewAutoresizing.None
            let view = landScapeVC.view
            var frame = view.frame
            frame.origin.y = 0
            frame.size.width = CGRectGetWidth(UIScreen.mainScreen().bounds)
            frame.size.height = self.view.frame.size.height
            view.frame = frame
            
            self.view.addSubview(view)
            
        } else {
            
            let portraitVC = self.storyboard!.instantiateViewControllerWithIdentifier("portrait")
            portraitVC.view.autoresizingMask = UIViewAutoresizing.None
            let view = portraitVC.view
            var frame = view.frame
            frame.origin.y = 0
            frame.size.width = CGRectGetWidth(UIScreen.mainScreen().bounds)
            frame.size.height = self.view.frame.size.height
            view.frame = frame
            
            self.view.addSubview(view)
            
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : Change orientation

    
    func rotated()
    {
        
        if UIDevice.currentDevice().orientation == UIDeviceOrientation.Portrait || UIDevice.currentDevice().orientation == UIDeviceOrientation.PortraitUpsideDown {
            self.view.subviews.forEach { $0.removeFromSuperview() }
            let portraitVC = self.storyboard!.instantiateViewControllerWithIdentifier("portrait")
            portraitVC.view.autoresizingMask = UIViewAutoresizing.None
            let view = portraitVC.view
            var frame = view.frame
            frame.origin.y = 0
            if CGRectGetHeight(UIScreen.mainScreen().bounds) > CGRectGetWidth(UIScreen.mainScreen().bounds) {
                frame.size.width = CGRectGetWidth(UIScreen.mainScreen().bounds)
                frame.size.height = self.view.frame.size.height
            } else {
                frame.size.width = CGRectGetHeight(UIScreen.mainScreen().bounds)
                frame.size.height = self.view.frame.size.width
            }
            view.frame = frame
            
            self.view.addSubview(view)
            
        } else if UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeLeft || UIDevice.currentDevice().orientation == UIDeviceOrientation.LandscapeRight {
            self.view.subviews.forEach { $0.removeFromSuperview() }
            let landScapeVC = self.storyboard!.instantiateViewControllerWithIdentifier("landscape")
            landScapeVC.view.autoresizingMask = UIViewAutoresizing.None
            let view = landScapeVC.view
            var frame = view.frame
            frame.origin.y = 0
            if CGRectGetHeight(UIScreen.mainScreen().bounds) > CGRectGetWidth(UIScreen.mainScreen().bounds) {
                frame.size.width = CGRectGetHeight(UIScreen.mainScreen().bounds)
                frame.size.height = self.view.frame.size.width
            } else {
                frame.size.width = CGRectGetWidth(UIScreen.mainScreen().bounds)
                frame.size.height = self.view.frame.size.height
            }
            view.frame = frame
            
            self.view.addSubview(view)

        }
        
    }
    
}
