//
//  CustomTabBarController.swift
//  TestOrientationSwift
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

import UIKit


class CustomTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tabBar = self.tabBar
        tabBar.translucent = false;
        
        let tabBarItem1 = tabBar.items![0]
        let tabBarItem2 = tabBar.items![1]
        let tabBarItem3 = tabBar.items![2]
        let tabBarItem4 = tabBar.items![3]
        let tabBarItem5 = tabBar.items![4]
        
        let tabBackgroundImage = UIImage.init(named: "nav_bg.png")
        tabBar.backgroundImage = tabBackgroundImage
        tabBar.clipsToBounds = true
        
        tabBarItem1.title = "Home"
        tabBarItem1.image = scaleToSize(UIImage.init(named:"icon1.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem1.selectedImage = scaleToSize(UIImage.init(named:"icon1.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem2.title = "Progress"
        tabBarItem2.image = scaleToSize(UIImage.init(named:"icon2.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem2.selectedImage = scaleToSize(UIImage.init(named:"icon2.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem3.title = ""
        tabBarItem3.image = scaleToSize(UIImage.init(named:"baby.png")!, size: CGSize.init(width: 40.0, height: 40.0)).imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem3.selectedImage = scaleToSize(UIImage.init(named:"baby.png")!, size: CGSize.init(width: 40.0, height: 40.0)).imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem4.title = "Catalogue"
        tabBarItem4.image = scaleToSize(UIImage.init(named:"icon3.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem4.selectedImage = scaleToSize(UIImage.init(named:"icon3.png")!, size: CGSize.init(width: 28.0, height: 28.0)).imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem5.title = "Milestones"
        tabBarItem5.image = scaleToSize(UIImage.init(named:"icon4.png")!, size: CGSize.init(width: 20.0, height: 32.0)).imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem5.selectedImage = scaleToSize(UIImage.init(named:"icon4.png")!, size: CGSize.init(width: 20.0, height: 32.0)).imageWithRenderingMode(.AlwaysOriginal)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor()], forState: UIControlState.Normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.whiteColor()], forState: UIControlState.Selected)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Scale to size
    func scaleToSize (originImage : UIImage, size : CGSize) -> UIImage {
        
        let hasAlpha = false
        let scale: CGFloat = 0.0
        
        UIGraphicsBeginImageContextWithOptions(size, hasAlpha, scale)
        originImage.drawInRect(CGRect(origin: CGPointZero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
        
    }
    

}
