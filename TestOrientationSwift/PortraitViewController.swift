//
//  PortraitViewController.swift
//  TestOrientationSwift
//
//  Created by Yatseyko Yura on 08.01.16.
//  Copyright © 2016 Yatseyko Yura. All rights reserved.
//

import UIKit

class PortraitViewController: UIViewController {

    
    @IBOutlet var fullDescriptionButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullDescriptionButton.layer.cornerRadius = 15.0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
